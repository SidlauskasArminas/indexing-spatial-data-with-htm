# SOURCES #


* http://skyserver.org/HTM/index.html - trumpas HTM Overview
* https://arxiv.org/ftp/cs/papers/0701/0701164.pdf - Indexing the Sphere with the Hierarchical Triangular Mesh
* http://www.cs.umd.edu/~hjs/pubs/LeeTODS00.pdf - Navigating through Triangle Meshes Implemented as Linear Quadtrees (psl. 86 Neighbor finding)
* http://www.skyserver.org/htm/implementation.aspx - SkyServer SpatialHTM kodo bibliotekos atsisiuntimas
* https://github.com/michaelleerilee/hstm/blob/master/src/SpatialIndex.cpp#L115 � C++ SkyServer HTM kodo biblioteka github'e
* https://uber.github.io/h3/#/documentation/core-library/unix-style-filters - Uber H3 dokumentacija
* "Addressing the Big-Earth-Data Variety Challenge with the Hierarchical Triangular Mesh" - NASA mokslinis straipsnis