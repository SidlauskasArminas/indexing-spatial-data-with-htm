#include "stdafx.h"
#include "CppUnitTest.h"

#define private public

#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/*Testuojama HTM2 (Daumantas Jankauskas)
  Realizuoti komand�, kuri sukurtu hierarchin� trikampi� tinklel�*/
namespace UnitTest1
{
	TEST_CLASS(HTMVariableTest)
	{
	public:
		TEST_METHOD(TestingStoredLeaveNumber1)
		{
			SpatialIndex si(0, 0);
			unsigned __int64 leaves = si.storedLeaves;
			unsigned __int64 ExpLeaves = 8;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingStoredLeaveNumber2)
		{
			SpatialIndex si(15, 10);
			unsigned __int64 leaves = si.storedLeaves;
			unsigned __int64 ExpLeaves = 8388608;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingLeaveNumber1)
		{
			SpatialIndex si(2, 1);
			unsigned __int64 leaves = si.leaves;
			unsigned __int64 ExpLeaves = 128;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingLeaveNumber2)
		{
			SpatialIndex si(4, 1);
			unsigned __int64 leaves = si.leaves;
			unsigned __int64 ExpLeaves = 2048;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingBaseLeaveNumber)
		{
			SpatialIndex si(0, 0);
			unsigned __int64 leaves = si.leaves;
			unsigned __int64 ExpLeaves = 8;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingLeaveNumber4)
		{
			SpatialIndex si(6, 1);
			unsigned __int64 leaves = si.leaves;
			unsigned __int64 ExpLeaves = 32768;
			Assert::AreEqual(ExpLeaves, leaves);
		}

		TEST_METHOD(TestingNumberOfNodes1)
		{
			SpatialIndex si(1, 0);
			unsigned __int64 nodes = si.nodes.size();
			unsigned __int64 ExpNodes = 8 * 4 + 8 + 1;
			Assert::AreEqual(ExpNodes, nodes);
		}

		TEST_METHOD(TestingNumberOfNodes2)
		{
			SpatialIndex si(5, 0);
			unsigned __int64 nodes = si.nodes.size();
			unsigned __int64 ExpNodes = 8192 + 2728 + 1;
			Assert::AreEqual(ExpNodes, nodes);
		}

		TEST_METHOD(TestingNumberOfVertices1)
		{
			SpatialIndex si(1, 0);
			unsigned __int64 vertices = si.vertices.size();
			unsigned __int64 ExpVertices = 12 + 6 + 1;
			Assert::AreEqual(ExpVertices, vertices);
		}

		TEST_METHOD(TestingNumberOfVertices2)
		{
			SpatialIndex si(6, 0);
			unsigned __int64 vertices = si.vertices.size();
			unsigned __int64 ExpVertices = 16386 + 1;
			Assert::AreEqual(ExpVertices, vertices);
		}

		TEST_METHOD(TestingNumberOfLevels1)
		{
			SpatialIndex si(5, 0);
			unsigned __int64 levels = si.layers.size();
			unsigned __int64 ExpLevels = 5 + 1;
			Assert::AreEqual(ExpLevels, levels);
		}

		TEST_METHOD(TestingNumberOfLevels2)
		{
			SpatialIndex si(2, 5);
			unsigned __int64 levels = si.layers.size();
			unsigned __int64 ExpLevels = 2 + 1;
			Assert::AreEqual(ExpLevels, levels);
		}
	};
}