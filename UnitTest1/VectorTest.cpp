#include "stdafx.h"
#include "CppUnitTest.h"

#define private public

#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM3 (Arminas Sidlauskas)
   Konvertuoti erdv�s koordinates � trikampi� kampus ant sferos (spatial vector)*/
namespace UnitTest1
{
	TEST_CLASS(VectorNormalizationTests)
	{
	public:
		TEST_METHOD(VectorNormalizationTest1)
		{
			SpatialVector vector;
			vector.set(2167.325, -2253.412, 5540.63);
			double sum = vector.x + vector.y + vector.z;
			double actual = 1.0;
			Assert::IsTrue(sum < actual);
		}

		TEST_METHOD(VectorNormalizationTest2)
		{
			SpatialVector vector;
			vector.set(-3799.45, -4294.967, -2783.139);
			double sum = vector.x + vector.y + vector.z;
			double actual = 1.0;
			Assert::IsTrue(sum < actual);
		}

		TEST_METHOD(VectorNormalizationTest3)
		{
			SpatialVector vector;
			vector.set(5008.072, 2332.753, -3176.493);
			double sum = vector.x + vector.y + vector.z;
			double actual = 1.0;
			Assert::IsTrue(sum < actual);
		}		
	};
}