#include "stdafx.h"
#include "CppUnitTest.h"
#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

/* Testuojama HTM0 (Gintaras Mi�eikis)
   Realizuoti komandas HTM indekso konvertavimui � skirtingus tipus*/
namespace UnitTest1
{
	TEST_CLASS(NameToIDTest)
	{
	public:

		TEST_METHOD(FormingCorrectIndexName1)
		{
			char ExpName[] = "S0123";
			char name[32];
			SpatialIndex::IndexToName(486388759756013571, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingCorrectIndexName2)
		{
			char ExpName[] = "N0123";
			char name[32];
			SpatialIndex::IndexToName(5098074778183401475, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingCorrectIndexName3)
		{
			char ExpName[] = "S01230";
			char name[32];
			SpatialIndex::IndexToName(486388759756013572, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(NegativeIndexExceptionTest)
		{
			auto func = [] {
				char name[32];
				return SpatialIndex::IndexToName(-1, name); 
			};
			Assert::ExpectException<std::invalid_argument>(func);
		}

		TEST_METHOD(BigIndexExceptionTest)
		{
			auto func = [] {
				char name[32];
				return SpatialIndex::IndexToName(9223372036854775808, name); 
			};
			Assert::ExpectException<std::invalid_argument>(func);
		}

		TEST_METHOD(FormingBaseIndexName1)
		{
			char ExpName[] = "S0";
			char name[32];
			SpatialIndex::IndexToName(0, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingBaseIndexName2)
		{
			char ExpName[] = "S1";
			char name[32];
			SpatialIndex::IndexToName(1152921504606846976, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingBaseIndexName3)
		{
			char ExpName[] = "N1";
			char name[32];
			SpatialIndex::IndexToName(5764607523034234880, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingBiggestIndexWithN)
		{
			char ExpName[] = "N3333333333333333333333333333";
			char name[32];
			SpatialIndex::IndexToName(9223372036854775771, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingBiggestIndexWithS)
		{
			char ExpName[] = "S3333333333333333333333333333";
			char name[32];
			SpatialIndex::IndexToName(4611686018427387867, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingCasualIndexName1)
		{
			char ExpName[] = "N0132321021312021300031210312";
			char name[32];
			SpatialIndex::IndexToName(5168205351127240091, name);
			Assert::AreEqual(ExpName, name);
		}

		TEST_METHOD(FormingCasualIndexName2)
		{
			char ExpName[] = "S123113333112100000000000003";
			char name[32];
			SpatialIndex::IndexToName(1972565160635663130, name);
			Assert::AreEqual(ExpName, name);
		}
	};
}