﻿#include "stdafx.h"
#include "CppUnitTest.h"
#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM7 (Gintaras Miðeikis)
   Realizuoti ilgumos ir plokštumos konvertavimą į xyz koordinates*/
namespace UnitTest1
{
	TEST_CLASS(LatLonToXyz)
	{
	public:
		TEST_METHOD(LatitudeExceptionTest1)
		{
			auto func = [] {return SpatialVector(-90.125, 0); };
			Assert::ExpectException<std::out_of_range>(func);
		}
		
		TEST_METHOD(LatitudeExceptionTest2)
		{
			auto func = [] {return SpatialVector(90.125, 0); };
			Assert::ExpectException<std::out_of_range>(func);
		}


		
		TEST_METHOD(LongtitudeExceptionTest1)
		{
			auto func = [] {return SpatialVector(0, -180.1); };
			Assert::ExpectException<std::out_of_range>(func);
		}

		TEST_METHOD(LongtitudeExceptionTest2)
		{
			auto func = [] {return SpatialVector(0, 180.1); };
			Assert::ExpectException<std::out_of_range>(func);
		}

		TEST_METHOD(CheckingLatBoundaries1)
		{
			SpatialVector expected(3.91849150799252 * pow(10,-13), 0, 6356.75231415482);
			SpatialVector actual(90, 0);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckingLatBoundaries2)
		{
			SpatialVector expected(3.91849150799252 * pow(10, -13), 0, -6356.75231415482);
			SpatialVector actual(-90, 0);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckingLonBoundaries1)
		{
			SpatialVector expected(-6378.137, 7.8107070957496*pow(10,-13), 0);
			SpatialVector actual(0, 180);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckingLonBoundaries2)
		{
			SpatialVector expected(-6378.137, -7.8107070957496*pow(10, -13), 0);
			SpatialVector actual(0, -180);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckCoordinates1)
		{

			SpatialVector expected(2694845.23286485*pow(10,-3), 3211591.48775206*pow(10, -3), pow(10, -3)*4790558.74740397);
			SpatialVector actual(49, 50);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckCoordinates2)
		{
			SpatialVector expected(-3.91849150799252*pow(10, -13), 4.79860959808667*pow(10, -27), (-1)*pow(10, -3)*6356752.31415482);
			SpatialVector actual(-90, 180);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckCoordinates3)
		{
			SpatialVector expected(-3.91849150799252*pow(10, -13), 4.79860959808667*pow(10, -27), -6356.75231415482);
			SpatialVector actual(-90, -180);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckCoordinates4)
		{
			SpatialVector expected(2231.9721490988, 3916.39838193446, -4497.16054802874);
			SpatialVector actual(-45.125, 60.321);
			Assert::IsTrue((expected == actual) == 1);
		}

		TEST_METHOD(CheckCoordinates5)
		{
			SpatialVector expected(6378.137, 0, 0);
			SpatialVector actual(0, 0);

			Assert::IsTrue((expected == actual) == 1);
		}
	};
}