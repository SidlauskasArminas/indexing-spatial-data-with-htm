#include "stdafx.h"
#include "CppUnitTest.h"

#define private public

#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM5 (Modestas Petraitis)
   Realizuoti komand�, kuri gr��ina erdvinius duomenis i� �vesto indekso*/
namespace UnitTest1
{
	TEST_CLASS(PointByIdTest)
	{
	public:
		TEST_METHOD(pointById_Test1_BaseIndex)
		{
			SpatialIndex si(2, 0);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 8070450532247928832;	//N3
			vector.set(3716.555, 948.704, 5078.776);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test2_BaseIndex)
		{
			SpatialIndex si(1, 0);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 4611686018427387904;	//N0
			vector.set(2167.325, -2253.412, 5540.63);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test3_BaseIndex)
		{
			SpatialIndex si(1, 0);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 0;						//S0
			vector.set(5008.072, 2332.753, -3176.493);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test4_BaseIndex)
		{
			SpatialIndex si(1, 0);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 2305843009213693952;	//S2
			vector.set(-3799.45, -4294.967, -2783.139);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_Level1)
		{
			SpatialIndex si(2, 0);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 3458764513820540929;	//S30
			vector.set(1772.606, -6118.998, -309.387);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test2_Level1)
		{
			SpatialIndex si(2, 1);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 7205759403792793601;	//N21
			vector.set(-1436.515, 1616.1, 5980.321);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test2_BuildLevel2)
		{
			SpatialIndex si(5, 2);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 7205759403792793601;	//N21
			vector.set(-1436.515, 1616.1, 5980.321);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_Level2)
		{
			SpatialIndex si(3, 2);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 8430738502437568514;	//N312
			vector.set(3435.848, 825.911, 5291.948);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test2_Level2)
		{
			SpatialIndex si(3, 1);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 3963167672086036482;	//S313
			vector.set(1256.552, -2117.799, -5863.864);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_MaxLevel5)
		{
			SpatialIndex si(5, 1);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 3963167672086036482;	//S313
			vector.set(1256.552, -2117.799, -5863.864);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_Level3)
		{
			SpatialIndex si(4, 1);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 5098074778183401475;	//N0123
			vector.set(3396.446, -445.897, 5362.106);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_MaxLevel6)
		{
			SpatialIndex si(6, 3);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 5098074778183401475;	//N0123
			vector.set(3396.446, -445.897, 5362.106);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}

		TEST_METHOD(pointById_Test1_ZeroCoordinates)
		{
			SpatialIndex si(4, 1);
			SpatialVector vector;
			SpatialVector CornerVectors[3];
			signed __int64 ID = 4;						//S0000
			vector.set(0, 0, 0);
			si.pointById(ID, CornerVectors);
			bool exp = si.isInsideCheck(vector, CornerVectors[0], CornerVectors[1], CornerVectors[2]);
			Assert::IsTrue(exp);
		}
	};
}