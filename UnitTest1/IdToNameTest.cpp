#include "stdafx.h"
#include "CppUnitTest.h"
#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM0 (Gintaras Mi�eikis)
   Realizuoti komandas HTM indekso konvertavimui � skirtingus tipus*/
namespace UnitTest1
{
	TEST_CLASS(IdToNameTest)
	{
	public:

		TEST_METHOD(FormingCorrectIndex1)
		{
			unsigned long long index = 486388759756013571;
			unsigned long long rindex = SpatialIndex::idByName("S0123000000000000000000000000", 3);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingCorrectIndex2)
		{
			unsigned long long index = 5098074778183401475;
			unsigned long long rindex = SpatialIndex::idByName("N0123000000000000000000000000", 3);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingCorrectIndex3)
		{
			unsigned long long index = 486388759756013572;
			unsigned long long rindex = SpatialIndex::idByName("S012300000000000000000000000", 4);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(OnlyIndexLetterGiven1)
		{
			unsigned long long index = -1;
			unsigned long long rindex = SpatialIndex::idByName("S", 0);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(OnlyIndexLetterGiven2)
		{
			unsigned long long index = -1;
			unsigned long long rindex = SpatialIndex::idByName("N", 0);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(EmptyNameExceptionTest)
		{
			auto func = [] {return SpatialIndex::idByName("", 0); };
			Assert::ExpectException<std::invalid_argument>(func);
		}

		TEST_METHOD(NameToLongExceptionTest) 
		{
			auto func = [] {return SpatialIndex::idByName("S00000000000000000000000000000", 0); };
			Assert::ExpectException<std::invalid_argument>(func);
		}

		TEST_METHOD(FormingBaseIndex1)
		{
			unsigned long long index = 0;
			unsigned long long rindex = SpatialIndex::idByName("S0000000000000000000000000000", 0);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingBaseIndex2)
		{
			unsigned long long index = 1152921504606846976;
			unsigned long long rindex = SpatialIndex::idByName("S1000000000000000000000000000", 0);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingBaseIndex3)
		{
			unsigned long long index = 5764607523034234880;
			unsigned long long rindex = SpatialIndex::idByName("N1000000000000000000000000000", 0);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingBigIndexWithN)
		{
			unsigned long long index = 9223372036854775771;
			unsigned long long rindex = SpatialIndex::idByName("N3333333333333333333333333333", 27);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingBigIndexWithS)
		{
			unsigned long long index = 4611686018427387867;
			unsigned long long rindex = SpatialIndex::idByName("S3333333333333333333333333333", 27);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingCasualIndex1)
		{
			unsigned long long index = 1972565160635662348;
			unsigned long long rindex = SpatialIndex::idByName("S1231133331121000000000000000", 12);
			Assert::AreEqual(index, rindex);
		}

		TEST_METHOD(FormingCasualIndex2)
		{
			unsigned long long index = 5168205351127240091;
			unsigned long long rindex = SpatialIndex::idByName("N0132321021312021300031210312", 27);
			Assert::AreEqual(index, rindex);
		}
	};
}