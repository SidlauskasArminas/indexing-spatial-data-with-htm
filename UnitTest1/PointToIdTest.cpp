#include "stdafx.h"
#include "CppUnitTest.h"
#include "../HTMindexing/SpatialIndex.h"
#include "../HTMindexing/SpatialVector.h"
#include "../HTMindexing/SpatialEdge.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM1 (Justas Radkevi�ius)
   Realizuoti komand�, kuri i� �vest� erdvini� duomen� gr��intu indeks�*/
namespace UnitTest1
{		
	TEST_CLASS(PointToIdTest)
	{
	public:
		TEST_METHOD(BaseIndexFromCoordinates1)
		{
			SpatialIndex si(0, 0);
			unsigned long long id = si.idByPoint(3716.555, 948.704, 5078.776);
			unsigned long long expId = 8070450532247928832;	//N3
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(BaseIndexFromCoordinates2)
		{
			SpatialIndex si(0, 0);
			unsigned long long id = si.idByPoint(2167.325, -2253.412, 5540.63);
			unsigned long long expId = 4611686018427387904; //N0
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(BaseIndexFromCoordinates3)
		{
			SpatialIndex si(0, 0);
			unsigned long long id = si.idByPoint(5008.072, 2332.753, -3176.493);
			unsigned long long expId = 0; //S0
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(BaseIndexFromCoordinates4)
		{
			SpatialIndex si(0, 0);
			unsigned long long id = si.idByPoint(-3799.45, -4294.967, -2783.139);
			unsigned long long expId = 2305843009213693952;	//S2
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(Forming2LevelIndexFromCoordinates1)
		{
			SpatialIndex si(1, 0);
			unsigned long long id = si.idByPoint(1772.606, -6118.998, -309.387);
			unsigned long long expId = 3458764513820540929;	//S30
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(Forming2LevelIndexFromCoordinates2)
		{
			SpatialIndex si(1, 0);
			unsigned long long id = si.idByPoint(-1436.515, 1616.1, 5980.321);
			unsigned long long expId = 7205759403792793601;	//N21
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(Forming3LevelIndexFromCoordinates1)
		{
			SpatialIndex si(2, 1);
			unsigned long long id = si.idByPoint(3435.848, 825.911, 5291.948);
			unsigned long long expId = 8430738502437568514;	//N312
			Assert::AreEqual(expId, id);
		}
		 
		TEST_METHOD(Forming3LevelIndexFromCoordinates2)
		{
			SpatialIndex si(2, 1);
			unsigned long long id = si.idByPoint(1256.552, -2117.799, -5863.864);
			unsigned long long expId = 3963167672086036482;	//S313
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(Forming4LevelIndexFromCoordinates)
		{
			SpatialIndex si(3, 4);
			unsigned long long id = si.idByPoint(3396.446, -445.897, 5362.106);
			unsigned long long expId = 5098074778183401475;	//N0123
			Assert::AreEqual(expId, id);
		}

		TEST_METHOD(ZeroCoordinates)
		{
			SpatialIndex si(4, 1);
			unsigned long long id = si.idByPoint(0, 0, 0);
			unsigned long long expId = 4; //S0000
			Assert::AreEqual(expId, id);
		}

	};
}