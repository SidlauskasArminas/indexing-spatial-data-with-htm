#include "stdafx.h"
#include <iostream>
#include <string>
#include "../HTMindexing/WKT.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

/* Testuojama HTM5 (Justas Radkevicius, Arminas Sidlauskas)
   Realizuoti komand�, kuri gr��ina erdvinius duomenis i� �vesto indekso*/
namespace UnitTest1
{
	TEST_CLASS(WKTTest)
	{
	public:
		TEST_METHOD(getPoint_Test1_BasePoint)
		{
			std::string WKTString(WKT::getPoint(3716.555, 948.704, 5078.776, 0)); //N3
			std::string expString("POINT(8070450532247928832)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getPoint_Test2_BasePoint)
		{
			std::string WKTString(WKT::getPoint(-3799.45, -4294.967, -2783.139, 0)); //S2
			std::string expString("POINT(2305843009213693952)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getPoint_Test1_PointTest)
		{
			std::string WKTString(WKT::getPoint(1256.552, -2117.799, -5863.864, 2)); //N0123
			std::string expString("POINT(3963167672086036482)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getPoint_Test2_PointTest)
		{
			std::string WKTString(WKT::getPoint(3396.446, -445.897, 5362.106, 3)); //S313
			std::string expString("POINT(5098074778183401475)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getMultiPoint_Test1_Level3Points)
		{
			const int NumberOfCoordinates = 3;
			double coordinates[NumberOfCoordinates][3] =
			{
				{3690.518, 1403.474, 4992.294},
				{6240.243, 1319.053, -9.956},
				{1042.749, 6219.882, 948.848}
			};
			std::string WKTString(WKT::getMultiPoint(NumberOfCoordinates, coordinates, 3)); //N3301, S0001, N3003
			std::string expString("MULTIPOINT((8971170457722028035) (36028797018963971) (8124493727776374787))");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getLineString_Test1_Level1Points)
		{
			const int NumberOfCoordinates = 2;
			double coordinates[NumberOfCoordinates][3] =
			{
				{-1436.515, 1616.1, 5980.321},
				{1772.606, -6118.998, -309.387}
			};
			std::string WKTString(WKT::getLineString(NumberOfCoordinates, coordinates, 1)); //N21 ir S30
			std::string expString("LINESTRING(7205759403792793601 3458764513820540929)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getLineString_Test2_Level3Points)
		{
			const int NumberOfCoordinates = 3;
			double coordinates[NumberOfCoordinates][3] =
			{
				{3690.518, 1403.474, 4992.294},
				{6240.243, 1319.053, -9.956},
				{1042.749, 6219.882, 948.848}
			};
			std::string WKTString(WKT::getLineString(NumberOfCoordinates, coordinates, 3)); //N3301, S0001, N3003
			std::string expString("LINESTRING(8971170457722028035 36028797018963971 8124493727776374787)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getPolygon_Test1_Level3Points)
		{
			const int NumberOfCoordinates = 4;
			double coordinates[NumberOfCoordinates][3] =
			{
				{3690.518, 1403.474, 4992.294},
				{6240.243, 1319.053, -9.956},
				{1042.749, 6219.882, 948.848},
				{3690.518, 1403.474, 4992.294}
			};
			std::string WKTString(WKT::getPolygon(NumberOfCoordinates, coordinates, 3)); //N3301, S0001, N3003, N3301
			std::string expString("POLYGON(8971170457722028035 36028797018963971 8124493727776374787 8971170457722028035)");
			Assert::AreEqual(expString, WKTString);
		}

		TEST_METHOD(getPolygon_Test1_ErrorTest)
		{
			auto func = [] {
				const int NumberOfCoordinates = 4;
				double coordinates[NumberOfCoordinates][3] =
				{
					{3690.518, 1403.474, 4992.294},
					{6240.243, 1319.053, -9.956},
					{1042.749, 6219.882, 948.848},
					{-2649.726, 5167.567, -2628.549}
				};
				std::string WKTString(WKT::getPolygon(NumberOfCoordinates, coordinates, 3)); //N3301, S0001, N3003,
				return WKTString;
			};
			Assert::ExpectException<std::invalid_argument>(func);
		}

		TEST_METHOD(getPolygon_Test2_Level10Points)
		{
			const int NumberOfCoordinates = 5;
			double coordinates[NumberOfCoordinates][3] =
			{
				{3690.518, 1403.474, 4992.294},
				{6240.243, 1319.053, -9.956},
				{1042.749, 6219.882, 948.848},
				{-601.134, 6120.459, -1685.264},
				{3690.518, 1403.474, 4992.294},
			};
			//N33021000123, S00021003100, N30030111203, S10013212300, N33021000123
			std::string WKTString(WKT::getPolygon(NumberOfCoordinates, coordinates, 10));
			std::string expString("POLYGON(8975703744163348490 40761095064911882 8126009954311077898 1187173490835324938 8975703744163348490)");
			Assert::AreEqual(expString, WKTString);
		}
	};
}