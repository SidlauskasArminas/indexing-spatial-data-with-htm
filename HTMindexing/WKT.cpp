#include "stdafx.h"
#include "WKT.h"
#include "SpatialIndex.h"

/* WKT:
	Constructor for the WKT class*/
WKT::WKT()
{
}

/* getPoint:
	Returns a POINT string with the given level node index in which the given coordinates are 
		> x, y, z - coordinates of the point
		> level - resolution of the index we are generating for the coordinates */
string WKT::getPoint(double x, double y, double z, int level)
{
	// check if level is valid
	if (level < 0 || level > 27)
	{
		throw std::invalid_argument("WKT:getpoint: level given is invalid");
	}

	// converts coordinates to index and forms the string to return
	SpatialIndex si(level, 1);
	signed __int64 index = si.idByPoint(x, y, z);
	std::string tmp("POINT(");
	tmp = tmp + std::to_string(index) + ')';
	return tmp;
}

/* getMultiPoint:
	Returns a MULTIPOINT string with the node indexes for every given coordinate 
		> NumberOfCoordiantes - gives the number of xyz coordintes in the array 
		> coordinates - array of coordinates that make up the multipoint geometry object
		> level - resolution of the index we are generating for the coordinates */
string WKT::getMultiPoint(const int NumberOfCoordinates, double coordinates[][3], int level)
{
	// check if level is valid
	if (level < 0 || level > 27)
	{
		throw std::invalid_argument("WKT:getMultiPoint: level given is invalid");
	}

	// converts coordinates to index and forms the string to return
	SpatialIndex si(level, 1);
	std::string returnString = "MULTIPOINT(";
	signed __int64 indexes;
	for (int n = 0; n < NumberOfCoordinates; n++)
	{
		indexes = si.idByPoint(coordinates[n][0], coordinates[n][1], coordinates[n][2]);
		returnString = returnString + '(' + std::to_string(indexes) + ')';
		if (n == NumberOfCoordinates - 1)
		{
			returnString = returnString + ')';
		}
		else
		{
			returnString = returnString + ' ';
		}
	}

	return returnString;
}


/* getLineString:
	Returns a LINE string with the given level node indexes, these nodes create the line 
		> NumberOfCoordiantes - gives the number of xyz coordintes in the array that create the line
		> coordinates - array of coordinates that make up the linestring geometry object
		> level - resolution of the index we are generating for the coordinates */
string WKT::getLineString(int NumberOfCoordinates, double coordinates[][3], int level)
{
	// check if level is valid
	if (level < 0 || level > 27)
	{
		throw std::invalid_argument("WKT:getpoint: level given is invalid");
	}

	// converts coordinates to index and forms the string to return
	SpatialIndex si(level, 1);
	std::string returnString = "LINESTRING(";
	signed __int64 indexes;
	for (int n = 0; n < NumberOfCoordinates; n++)
	{
		indexes = si.idByPoint(coordinates[n][0], coordinates[n][1], coordinates[n][2]);
		returnString = returnString + std::to_string(indexes);
		if (n == NumberOfCoordinates - 1)
		{
			returnString = returnString + ')';
		}
		else 
		{
			returnString = returnString + ' ';
		}
	}

	return returnString;
}

/* getPolygon:
	Returns a POLYGON string with the indexes of node created with the given coordinates and level
	Nodes are vertices of the POLYGON
		> NumberOfCoordiantes - gives the number of xyz coordintes in the array that create the line
		> coordinates - array of coordinates that make up the polygon geometry object
		> level - resolution of the index we are generating for the coordinates */
string WKT::getPolygon(const int NumberOfCoordinates, double coordinates[][3], int level)
{
	// check if level is valid
	if (level < 0 || level > 27)
	{
		throw std::invalid_argument("WKT:getpoint: given level is invalid");
	}

	// checks if the number of coordinates given is valid
	if (NumberOfCoordinates < 2)
	{
		throw std::invalid_argument("WKT:getPolygon: given number of points is invalid. Must be more than 2");
	}
	
	int nr = NumberOfCoordinates - 1;
	if (coordinates[0][0] != coordinates[nr][0] || coordinates[0][1] != coordinates[nr][1] || coordinates[0][2] != coordinates[nr][2])
	{
		throw std::invalid_argument("WKT:getPolygon: first and last points are not the same. Polygon is not connected");
	}

	// converts coordinates to index and forms the string to return
	SpatialIndex si(level, 1);
	std::string returnString = "POLYGON(";
	signed __int64 indexes;
	for (int n = 0; n < NumberOfCoordinates; n++)
	{
		indexes = si.idByPoint(coordinates[n][0], coordinates[n][1], coordinates[n][2]);
		returnString = returnString + std::to_string(indexes);
		if (n == NumberOfCoordinates - 1)
		{
			returnString = returnString + ')';
		}
		else
		{
			returnString = returnString + ' ';
		}
	}

	return returnString;
}
