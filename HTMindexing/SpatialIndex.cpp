//#     Filename:       SpatialIndex.cpp
//#
//#     The SpatialIndex class is defined here.
//#
//#     Author:         Peter Z. Kunszt based on A. Szalay's code
//#     
//#     Date:           October 15, 1998
//#
//#		Copyright (C) 2000  Peter Z. Kunszt, Alex S. Szalay, Aniruddha R. Thakar
//#                     The Johns Hopkins University
//#
//#     Modification History:
//#
//#     Oct 18, 2001 : Dennis C. Dinge -- Replaced ValVec with std::vector
//#		Jul 25, 2002 : Gyorgy Fekete -- Added pointById()
//#

#include "stdafx.h"
#include "SpatialIndex.h"
#include <stdio.h>
#include <math.h>
#include <stdexcept>
#include <climits>

#ifdef _WIN32
	#include <malloc.h>
	#else
	#ifdef __macosx
		#include <sys/malloc.h>
		#else
		#include <stdlib.h>
	#endif
#endif

#define N(x) nodes[(x)]							
#define V(x) vertices[nodes[index].v[(x)]]		// gets the vertex by the nodes vertex index (trikampio virsune x is virsuniu masyvo)
#define IV(x) nodes[index].v[(x)]				// gets the nodes vertex x index (trikampio virsunes x indeksa)
#define W(x) vertices[nodes[index].w[(x)]]		// gets the vertex by the nodes edge midpoint index (trikampio krastines vidurio taska x is virsuniu masyvo)
#define IW(x) nodes[index].w[(x)]				// gets the nodes edges midpoint x index (trikampio x krastines vidurio taska)
#define ICHILD(x) nodes[index].childID[(x)]		// gets the nodes child x trianges' index (trikampio vaiko x indeksa)
#define IV_(x) nodes[index_].v[(x)]				// gets the nodes x vertex vector (trikampio x virsune)				
#define ICHILD_(x) nodes[index_].childID[(x)]	// gets the nodes child x trianges' index (trikampio vaiko x indeksa)
#define IOFFSET 9

/* 1) SpatialIndex: 
	empty constructor */
SpatialIndex::SpatialIndex()
{
}

/* 2) SpatialIndex:
	constructor for buiding the HTM 
		> maxlevel - biggest resolution level for generating indices 
		> buildlevel - number of layers that will be stored in memory */
SpatialIndex::SpatialIndex(size_t maxlevel_, size_t buildlevel_)
{
	//saving maxlevel of HTM
	maxlevel = maxlevel_;

	//if buildlevel is 0 or bigger then maxlevel then it is set to maxlevel
	buildlevel = (buildlevel_ == 0 || buildlevel_ > maxlevel) ? maxlevel : buildlevel_;

	size_t nodes_ = 0;
	size_t vertices_ = 0;

	vMax(&nodes_, &vertices_);

	// allocates enough space
	layers.resize(buildlevel + 1);
	// allocates space for all nodes
	nodes.resize(nodes_ + 1);
	// allocates space for all vertices
	vertices.resize(vertices_ + 1);

	// initialize invalid node
	nodes[0].index_ = 0;

	//initialize first layer
	layers[0].level = 0;
	layers[0].nVert = 6;
	layers[0].nNode = 8;
	layers[0].nEdge = 12;
	layers[0].firstIndex = 1;
	layers[0].firstVertex = 0;

	// set the first 6 vertices
	double v[6][3] = {
	  {0.0L,  0.0L,  1.0L},		// 0
	  {1.0L,  0.0L,  0.0L},		// 1
	  {0.0L,  1.0L,  0.0L},		// 2
	  {-1.0L,  0.0L,  0.0L},	// 3
	  {0.0L, -1.0L,  0.0L},		// 4
	  {0.0L,  0.0L, -1.0L}		// 5
	};

	//setting the first 6 vertices as HTM vectors
	for (int i = 0; i < 6; i++)
	{
		vertices[i].set(v[i][0], v[i][1], v[i][2]);
	}

	// create the first 8 nodes - index 1 through 8
	index_ = 1;
	newNode(1, 5, 2, 8, 0);		// S0
	newNode(2, 5, 3, 9, 0);		// S1
	newNode(3, 5, 4, 10, 0);	// S2
	newNode(4, 5, 1, 11, 0);	// S3
	newNode(1, 0, 4, 12, 0);	// N0
	newNode(4, 0, 3, 13, 0);	// N1
	newNode(3, 0, 2, 14, 0);	// N2
	newNode(2, 0, 1, 15, 0);	// N3

	// loop through buildlevel steps, and build the nodes for each layer
	size_t pl = 0;
	size_t level = buildlevel;
	while (level-- > 0)
	{
		SpatialEdge edge(*this, pl);
		edge.makeMidPoints();
		makeNewLayer(pl);
		++pl;
	}

	//sorts all nodes in ascending id-order
	sortIndex();
}

/* newNode:
	makes a new node. Saves the nodes vertices, id and parent ID to the node array
		> v1, v2, v3 - indices of the nodes vertices, they point to the nodes vertices in the list of all the vertices
		> id - nodes id to store
		> parent - nodes parents id to store */
__int64 SpatialIndex::newNode(size_t v1, size_t v2, size_t v3, signed __int64 id, signed __int64 parent)
{
	// vertex indices
	nodes[index_].v[0] = v1;
	nodes[index_].v[1] = v2;
	nodes[index_].v[2] = v3;

	// middle point indices
	nodes[index_].w[0] = 0;
	nodes[index_].w[1] = 0;
	nodes[index_].w[2] = 0;

	ICHILD_(0) = 0;					// child indices
	ICHILD_(1) = 0;					// index 0 is invalid node.
	ICHILD_(2) = 0;
	ICHILD_(3) = 0;

	N(index_).id = id;				// set the id
	N(index_).index_ = index_;		// set the index
	N(index_).parent = parent;		// set the parent

	return index_++;
}

/* makeNewLayer: 
	generate a new layer and the nodes in it 
		> oldlayer - the number of the old layer, new layers' number will be oldlayer + 1 */
void SpatialIndex::makeNewLayer(size_t oldlayer)
{
	signed __int64 index, id_;

	// sets all variables for new level
	size_t newlayer = oldlayer + 1;
	layers[newlayer].level = layers[oldlayer].level + 1;
	layers[newlayer].nVert = layers[oldlayer].nVert + layers[oldlayer].nEdge;
	layers[newlayer].nNode = 4 * layers[oldlayer].nNode;
	layers[newlayer].nEdge = layers[newlayer].nNode + layers[newlayer].nVert - 2;
	layers[newlayer].firstIndex = index_;
	layers[newlayer].firstVertex = layers[oldlayer].firstVertex + layers[oldlayer].nVert;

	signed __int64 ioffset = layers[oldlayer].firstIndex;

	// creates new nodes for the layer
	for (index = ioffset; index < ioffset + layers[oldlayer].nNode; index++)
	{
		id_ = N(index).id << 2;
		ICHILD(0) = newNode(IV(0), IW(2), IW(1), id_++, index);
		ICHILD(1) = newNode(IV(1), IW(0), IW(2), id_++, index);
		ICHILD(2) = newNode(IV(2), IW(1), IW(0), id_++, index);
		ICHILD(3) = newNode(IW(0), IW(1), IW(2), id_, index);
	}
}

/* vMax: 
	compute the maximum number of vertices for the
	polyhedron after buildlevel of subdivisions and
	the total number of nodes that we store
	also, calculate the number of leaf nodes that we eventually have. 
		> nodes - for storing and returning the number of nodes 
		> vertices - for storing and returning the number of vertices */
void SpatialIndex::vMax(size_t * nodes, size_t * vertices)
{
	signed __int64 nv = 6;				// first 6 vertices
	signed __int64 ne = 12;				// we get 12 new vertices when we divide the node
	signed __int64 nf = 8;				// first 8 nodes
	size_t i = buildlevel;
	*nodes = (size_t)nf;

	while (i-- > 0)
	{
		nv = nv + ne;
		nf = nf * 4;
		ne = nf + nv - 2;
		*nodes = *nodes + (size_t)nf;
	}

	*vertices = (size_t)nv;
	storedLeaves = nf;

	// calculate number of leaves
	i = maxlevel - buildlevel;
	while (i-- > 0)
	{
		nf = nf * 4;
	}

	leaves = nf;
}

/* InitialidByName:
	Right justified HTM node index name conversion to integer ID 
		> name - the index name (e. g. S1230) */
signed __int64 SpatialIndex::InitialidByName(char *name) const
{
	unsigned __int64 out = 0, i;
	size_t size = 0;
	size = strlen(name);

	if (name == 0)									// null pointer-name
		std::cout << "name == 0";
	if (name[0] != 'N' && name[0] != 'S')			// invalid name
		std::cout << "Does not start with N or S";

	if (size < 2)
		std::cout << "size < 2";					// the lenght of the index name is too small
	if (size > HTMNAMEMAX)
		std::cout << "size > Max";					// the lenght of the index name is too big

	for (i = size - 1; i > 0; i--)
	{
		if (name[i] > '3' || name[i] < '0')
			std::cout << "Invalid name digit";		// digit is not 0, 1, 2 or 3

		//shifts bits to the left according to the specific node number
		out = out + (__int64(name[i] - '0') << 2 * (size - i - 1));
	}

	i = 2;
	if (name[0] == 'N')								// checking the first char in the index name (is it N or S)
		i++;

	out = out + (i << (2 * size - 2));

	return out;
}

/* idByName:
	Left justified HTM node index name conversion to integer ID 
		> name - the index name (e. g. S1230)
		> NameLevel - the index resolution, it will be added to the index, last 5 bits will show the resolution */
signed __int64 SpatialIndex::idByName(const char *name, _int64 NameLevel)
{
	std::string t = std::string(name);

	if (t.length() > 29) {
		throw std::invalid_argument("SpatialIndex: idByName: Name string is too long");
	}

	if (t.length() == 0) {
		throw std::invalid_argument("SpatialIndex: idByName: Name string empty");
	}

	if (t.length() < 2)
		return -1;

	signed __int64 longIndex = 0;
	if (t[0] == 'N')
		longIndex += 1;

	// needed for counting resolution
	int lastNonZero = 0;

	for (int i = 1; i <= t.length() - 1; i++)
	{
		int digit = (int)(t[i]) - 48;

		if (digit > 3)
			return -1;

		if (digit > 0)
			lastNonZero = i;

		longIndex <<= 2;
		longIndex += digit;
	}

	longIndex = longIndex << (62 - (t.length() - 1) * 2);
	longIndex = longIndex + NameLevel;

	return longIndex;
}

/* InitialNameById:
	 Right justified HTM index ID is converted to a string name
	 The encoding described above may be decoded again using the following
	 procedure:
		* Traverse the uint64 from left to right.
		* Find the first 'true' bit.
		* The first pair gives N (01) or S (00).
		* The subsequent bit-pairs give the numbers 0-3. 
		
		> ID - index for which we are generating the name
		> name - for storing and returning the index name */
char * SpatialIndex::InitialNameById(signed __int64 ID, char * name)
{
	__int64 size = 0;
	__int64	i = 0;

	#ifdef _WIN32
		__int64 IDHIGHBIT = 1;
		__int64 IDHIGHBIT2 = 1;
		IDHIGHBIT = IDHIGHBIT << 63;
		IDHIGHBIT2 = IDHIGHBIT2 << 62;
	#endif

	// determine index of first set bit
	for (i = 0; i < IDSIZE; i += 2)
	{
		if ((ID << i) & IDHIGHBIT)
			break;

		// invalid ID
		if ((ID << i) & IDHIGHBIT2)
			throw std::invalid_argument("SpatialIndex:nameById: invalid ID");
	}

	if (ID == 0)
		throw std::invalid_argument("SpatialIndex:nameById: invalid ID");

	size = ((IDSIZE - i) >> 1);

	// allocate characters
	if (!name)
		name = new char[size + 1];

	// fill characters starting with the last one
	for (i = 0; i < size - 1; i++)
	{
		name[size - i - 1] = '0' + char((ID >> i * 2) & 3);
	}

	// put in first character
	if ((ID >> (size * 2 - 2)) & 1)
		name[0] = 'N';
	else
		name[0] = 'S';

	// end string
	name[size] = 0;

	return name;
}

/*IndexToName:
	Left justified HTM index ID conversion to a string name 
		> ID - index for which we are generating the name
		> name - for storing and returning the index name */
char * SpatialIndex::IndexToName(signed __int64 ID, char * name)
{
	//checking if the ID value is valid
	if (ID < 0) {
		throw std::invalid_argument("IndexToName: ID value given is lower then 0");
	}

	if (ID > LLONG_MAX) {
		throw std::invalid_argument("IndexToName: ID value given TOO BIG");
	}

	//setting the start of the index name
	name[0] = 'S';
	if ((1ll << (62)) & ID)
		name[0] = 'N';

	//cycles through the ID bits, checks them and add an appropriate number to the name
	for (int i = 61; i > 6; i = i - 2)
	{
		char a1 = '0', a2 = '0';
		int arrayIndex = 31 - i / 2;

		if ((1ll << i) & ID)
			a1 = '1';

		if ((1ll << (i - 1)) & ID)
			a2 = '1';

		if (a1 == '0' && a2 == '0')
			name[arrayIndex] = '0';

		if (a1 == '0' && a2 == '1')
			name[arrayIndex] = '1';

		if (a1 == '1' && a2 == '0')
			name[arrayIndex] = '2';

		if (a1 == '1' && a2 == '1')
			name[arrayIndex] = '3';
	}

	//gets the index resolution and ends the index name
	int res = ID << 58 >> 58;
	name[res + 2] = '\0';

	return name;
}

/* 1) idByPoint:
	Returns the integer ID of a node in which the given vector is
		> v - vertex/vector for which we are generating the ID */
signed __int64 SpatialIndex::idByPoint(SpatialVector& v) const
{
	signed __int64 index;	//index used for getting and checking node values from arrays
	signed __int64 ID;		//ID value will be generated and returned by the function

	// start with the 8 root triangles, find the one which v points to
	for (index = 1; index <= 8; index++)
	{
		if ((vertices[nodes[index].v[0]] ^ vertices[nodes[index].v[1]]) * v < gEpsilon)
			continue;
		if ((vertices[nodes[index].v[1]] ^ vertices[nodes[index].v[2]]) * v < gEpsilon)
			continue;
		if ((vertices[nodes[index].v[2]] ^ vertices[nodes[index].v[0]]) * v < gEpsilon)
			continue;
		break;
	}

	// loop through matching child until leaves are reached
	while (nodes[index].childID[0] != 0) 
	{
		signed __int64 oldindex = index;
		for (size_t i = 0; i < 4; i++)
		{
			index = nodes[oldindex].childID[i];
			if ((vertices[nodes[index].v[0]] ^ vertices[nodes[index].v[1]]) * v < -gEpsilon)
				continue;
			if ((vertices[nodes[index].v[1]] ^ vertices[nodes[index].v[2]]) * v < -gEpsilon)
				continue;
			if ((vertices[nodes[index].v[2]] ^ vertices[nodes[index].v[0]]) * v < -gEpsilon)
				continue;
			break;
		}
	}

	// returns id if we have reached maxlevel
	if (maxlevel == buildlevel) 
	{
		char name[IDSIZE];
		InitialNameById(nodes[index].id, name);
		ID = idByName(name, buildlevel);
		return ID;
	}

	//forming the name
	char name[IDSIZE];
	InitialNameById(nodes[index].id, name);
	size_t len = strlen(name);

	SpatialVector v0 = vertices[nodes[index].v[0]];
	SpatialVector v1 = vertices[nodes[index].v[1]];
	SpatialVector v2 = vertices[nodes[index].v[2]];

	//appending the number of the node to the index
	size_t level = maxlevel - buildlevel;

	while (level--) 
	{
		SpatialVector w0 = v1 + v2;
		w0.normalize();
		SpatialVector w1 = v0 + v2;
		w1.normalize();
		SpatialVector w2 = v1 + v0;
		w2.normalize();

		//if coordinate is inside the node, the number is added to the end of index
		//and the three main coordinates are adjusted, so that on the next level
		//the smaller tringle is checked
		if (isInside(v, v0, w2, w1)) {
			name[len++] = '0';
			v1 = w2;
			v2 = w1;
			continue;
		}
		else if (isInside(v, v1, w0, w2)) {
			name[len++] = '1';
			v0 = v1;
			v1 = w0;
			v2 = w2;
			continue;
		}
		else if (isInside(v, v2, w1, w0)) {
			name[len++] = '2';
			v0 = v2;
			v1 = w1;
			v2 = w0;
			continue;
		}
		else if (isInside(v, w0, w1, w2)) {
			name[len++] = '3';
			v0 = w0;
			v1 = w1;
			v2 = w2;
			continue;
		}
	}

	name[len] = '\0';
	ID = idByName(name, maxlevel);

	return ID;
}

/* 2) idByPoint: 
	Returns the integer ID of a node in which the given xyz coordintes are 
		> x, y, z - point coordinates for which the methd will generate the ID */
signed __int64 SpatialIndex::idByPoint(double x, double y, double z) const
{
	SpatialVector *vector = new SpatialVector(x, y, z);
	return idByPoint(*vector);
}

/* 3) idByPoint:
	Returns the integer ID of a node in which the given lat and lon coordintes are
		> lat, lon - point coordinates for which the methd will generate the ID */
signed __int64 SpatialIndex::idByPoint(double lat, double lon) const
{
	SpatialVector *vector = new SpatialVector(lat, lon);
	return idByPoint(*vector);
}

/* pointById:
	Find a vector for the leaf node given by its ID 
		> ID - index of a node we will be returning (node vertices we will be returning, actually)
		> vectors - the three vertices of the node that the ID points to */
void SpatialIndex::pointById(signed __int64 ID, SpatialVector vectors[3]) const
{
	char name[HTMNAMEMAX];
	signed __int64 id;
	SpatialVector v0, v1, v2;

	// getting the right justified index to get the HTM node with it
	IndexToName(ID, name);
	id = InitialidByName(name);

	//we do a bitwise shift and add 8 to get the base ID (this ID Right Justified Mapped)
	int BaseIndex = ID >> 60;
	BaseIndex += 8;

	//getting the index level from last 6 bits
	int Level = ID << 58 >> 58;

	//getting the BaseIndex from the array to get the vertices
	for (int i = 0; i < 9; i++)
	{
		if (nodes[i].id == BaseIndex)
		{
			BaseIndex = i;
			break;
		}
	}

	this->nodeVertex(id, BaseIndex, Level, v0, v1, v2);

	//sets the vertices of the node to which the ID points
	vectors[0] = v0;
	vectors[1] = v1;
	vectors[2] = v2;
}

/*sortIndex: 
	sort the index so that the first node is the invalid node
	(index 0), the next 8 nodes are the root nodes
    and then we put all the leaf nodes in the following block
    in ascending id-order.
    All the rest of the nodes are at the end.*/
void SpatialIndex::sortIndex()
{
	// create a copy of the node list
	ValueVectorQuad oldnodes(nodes);
	size_t index;
	size_t nonleaf;
	size_t leaf;

	#define ON(x) oldnodes[(x)]

	// now refill the nodes list according to our sorting
	for (index = IOFFSET, leaf = IOFFSET, nonleaf = nodes.size() - 1; index < nodes.size(); index++)
	{

		// childnode
		if (oldnodes[index].childID[0] == 0)
		{
			// set leaf into list
			N(leaf) = oldnodes[index];

			// set parent's pointer to this leaf
			for (size_t i = 0; i < 4; i++)
			{
				if (N(N(leaf).parent).childID[i] == index)
				{
					N(N(leaf).parent).childID[i] = leaf;
					break;
				}
			}

			leaf++;
		}
		else
		{
			// set nonleaf into list from the end
			// set parent of the children already to this
			// index, they come later in the list
			N(nonleaf) = ON(index);
			ON(N(nonleaf).childID[0]).parent = nonleaf;
			ON(N(nonleaf).childID[1]).parent = nonleaf;
			ON(N(nonleaf).childID[2]).parent = nonleaf;
			ON(N(nonleaf).childID[3]).parent = nonleaf;

			// set parent's pointer to this leaf
			for (size_t i = 0; i < 4; i++)
			{
				if (N(N(nonleaf).parent).childID[i] == index)
				{
					N(N(nonleaf).parent).childID[i] = nonleaf;
					break;
				}
			}
			nonleaf--;

		}
	}
}

/* isInside:
	Checks if the point is in the triangle created by points v0, v1, v2 
		> v - we are checking if vertex is in the node (v0, v1, v2 are the nodes' vertices)
		> v0, v1, v2 - the three vertices of a node */
bool SpatialIndex::isInside(const SpatialVector & v, const SpatialVector & v0, const SpatialVector & v1, const SpatialVector & v2) const
{
	if ((v0 ^ v1) * v < -gEpsilon)		
		return false;
	if ((v1 ^ v2) * v < -gEpsilon) 
		return false;
	if ((v2 ^ v0) * v < -gEpsilon) 
		return false;
	return true;
}

/* isInsideCheck:
	USED FOR TESTING. Same as isInside, but public and not const.
	Checks if the point is in the triangle created by points v0, v1, v2. 
		> v - we are checking if vertex is in the node (v0, v1, v2 are the nodes' vertices)
		> v0, v1, v2 - the three vertices of a node */
bool SpatialIndex::isInsideCheck(const SpatialVector & v, const SpatialVector & v0, const SpatialVector & v1, const SpatialVector & v2)
{
	if ((v0 ^ v1) * v < -gEpsilon)
		return false;
	if ((v1 ^ v2) * v < -gEpsilon)
		return false;
	if ((v2 ^ v0) * v < -gEpsilon)
		return false;
	return true;
}

/* nodeVertex:
	returns the vertices of the ids' node
		> id - index of the node we are looking for
		> BaseIndex - the index of the base 8 nodes, for finding the node if buildlevel != maxlevel 
		> level - the index level (resolution in other words)
		> v0, v1, v2 - the three vertices of a node to be saved and returned */
void SpatialIndex::nodeVertex(const signed __int64 id, int BaseIndex, int level, SpatialVector & v0, SpatialVector & v1, SpatialVector & v2) const
{
	// if the build level is equal to the max level all nodes are stored 
	// and we can just find the node by cycling through the array
	if (buildlevel == maxlevel)
	{
		for (size_t i = 0; i < nodes.size(); i++)
		{
			if (nodes[i].id == id)
			{
				v0 = vertices[nodes[i].v[0]];
				v1 = vertices[nodes[i].v[1]];
				v2 = vertices[nodes[i].v[2]];
				return;
			}
		}

		return;
	}

	// buildlevel < maxlevel
	// idx - array index of the base node that we are in
	// NumberOfLevels - number of level to cycle through
	unsigned __int64 idx = BaseIndex;
	size_t NumberOfLevels = level + 1;

	v0 = vertices[nodes[idx].v[0]];
	v1 = vertices[nodes[idx].v[1]];
	v2 = vertices[nodes[idx].v[2]];

	// loop therough additional levels,
	// pick the correct triangle accordingly, storing the vertices in v1,v2,v3
	for (size_t i = 2; i <= NumberOfLevels; i++)
	{
		unsigned __int64 j = (id >> ((NumberOfLevels - i) * 2)) & 3;
		SpatialVector w0 = v1 + v2; 
		w0.normalize();
		SpatialVector w1 = v0 + v2; 
		w1.normalize();
		SpatialVector w2 = v1 + v0; 
		w2.normalize();

		switch (j)
		{
			case 0:
				v1 = w2;
				v2 = w1;
				break;
			case 1:
				v0 = v1;
				v1 = w0;
				v2 = w2;
				break;
			case 2:
				v0 = v2;
				v1 = w1;
				v2 = w0;
				break;
			case 3:
				v0 = w0;
				v1 = w1;
				v2 = w2;
				break;
		}
	}
}