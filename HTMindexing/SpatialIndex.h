//#     Filename:       SpatialIndex.h
//#
//#     SpatialIndex is the class for the the sky indexing routines.
//#
//#     Author:         Peter Z. Kunszt, based on A. Szalay s code
//#
//#     Date:           October 15, 1998
//#
//#		Copyright (C) 2000  Peter Z. Kunszt, Alex S. Szalay, Aniruddha R. Thakar
//#                     The Johns Hopkins University
//#     Modification History:
//#
//#     Oct 18, 2001 : Dennis C. Dinge -- Replaced ValVec with std::vector
//#	    Sept 9, 2002 : Gyorgy Fekete -- added setMaxlevel()
//#

#pragma once

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <vector>
#include <string>
#include <algorithm>
#include "SpatialVector.h"
#include "SpatialEdge.h"

/*
   The Spatial Index is a quad tree of spherical triangles. The tree
   is built in the following way: Start out with 8 triangles on the
   sphere using the 3 main circles to determine them. Then, every
   triangle can be decomposed into 4 new triangles by drawing main
   circles between midpoints of its edges:

<pre>

.                            /\
.                           /  \
.                          /____\
.                         /\    /\
.                        /  \  /  \
.                       /____\/____\

</pre>

   This is how the quad tree is built up to a certain level by
   decomposing every triangle again and again.
*/

// Global Math Constants
const double gPi = 3.1415926535897932385E0;
const double gPr = gPi / 180.0;
const double gEpsilon = 1.0E-15;

#define IDSIZE		64
#define HTMNAMEMAX  32

class SpatialIndex
{
public:
	/*	Constructors.
		Give the level of the index and optionally the level to build (the depth to keep in memory).
		if(maxlevel - buildlevel > 0) that many levels are generated on the fly each time the index is called */
	SpatialIndex();
	SpatialIndex(size_t maxlevel_, size_t buildlevel = 5);

	// Right justified HTM node index name conversion to integer ID
	signed __int64 InitialidByName(char *name) const;

	// Left justified HTM node index name conversion to integer ID
	static signed __int64 idByName(const char *name, _int64 NameLevel);

	/* Right justified HTM index ID is converted to a string name

	  WARNING: if name is already allocated, a size of at least 17 is
	  required.  The conversion is done by directly calculating the
	  name from a number.  

	  To calculate the name of a certain level,
	  the mechanism is that the name is given by (#of nodes in that
	  level) + (id of node).  
	  
	  So for example, for the first level,
	  there are 8 nodes, and we get the names from numbers 8 through
	  15 giving S0,S1,S2,S3,N0,N1,N2,N3.  The order is always
	  ascending starting from S0000.. to N3333...  */
	static char * InitialNameById(signed __int64 ID, char * name = 0);

	// Left justified HTM index ID conversion to a string name
	static char * IndexToName(signed __int64 ID, char * name);

	// Find a node by given a vector. The ID of the node is returned
	signed __int64 idByPoint(SpatialVector& vector) const;

	// Find a node by given xyz coordinates. The ID of the node is returned
	signed __int64 idByPoint(double x, double y, double z) const;

	// Find a node by given latitude and longitude of a point. The ID of the node is returned
	signed __int64 idByPoint(double lat, double lon) const;

	// Find the three vectors of the triangle represented by the ID
	void pointById(signed __int64 ID, SpatialVector vectors[3]) const;

	// USED FOR TESTING. Same as isInside, but public and not const.
	// Test whether a vector v is inside a triangle v0,v1,v2. Input 
	// triangle has to be sorted in a counter-clockwise direction.
	bool isInsideCheck(const SpatialVector & v, const SpatialVector & v0, const SpatialVector & v1, const SpatialVector & v2);

	// Returns the depth(gylis) of the layer
	size_t getMaxLevel() 
	{
		return maxlevel;
	}

	// Returns the depth(gylis) of the layer stored
	size_t getBuildLevel() 
	{
		return buildlevel;
	}

private:
	struct Layer 
	{
		size_t level;					// layer level
		size_t nVert;					// number of vertices in the layer
		signed __int64 nNode;			// number of nodes
		size_t nEdge;					// number of edges
		signed __int64 firstIndex;		// index of first node of this layer
		size_t firstVertex;				// index of first vertex of this layer
	};

	struct QuadNode 
	{
		unsigned __int64 index_;		// its own index
		size_t v[3];					// the three vertex vector indices
		size_t w[3];					// the three middlepoint vector indices
		signed __int64 childID[4];		// ids of children
		signed __int64 parent;			// id of The parent node (for sorting)
		signed __int64 id;				// numeric id -> name
	};

	// list for nodes
	typedef std::vector<QuadNode> ValueVectorQuad;

	// VARIABLES

	size_t maxlevel;						// the depth(gylis) of the layer
	size_t buildlevel;						// the depth(gylis) of the layer stored
	signed __int64 leaves;					// number of leaf nodes (number of max level nodes)
	signed __int64 storedLeaves;			// number of stored leaf nodes (number of buildlevel nodes)
	ValueVectorQuad nodes;					// the array of nodes
	std::vector<Layer> layers;				// array of layers (buildlevel layers saved)

	typedef std::vector<SpatialVector> ValueVectorSpvec;
	ValueVectorSpvec vertices;				// list for vertices
	signed __int64 index_;					// the current index of vertices

	// FUNCTIONS

	// insert a new node_[] into the list. The vertex indices are given by
	// v1,v2,v3 and the id of the node is set.
	__int64 newNode(size_t v1, size_t v2, size_t v3, signed __int64 id, signed __int64 parent);

	// make new nodes in a new layer.
	void makeNewLayer(size_t oldlayer);

	// return the total number of nodes and vertices
	void vMax(size_t *nodes, size_t *vertices);

	// sort the index so that the leaf nodes are at the beginning
	void sortIndex();

	// Test whether a vector v is inside a triangle v0,v1,v2. Input 
	// triangle has to be sorted in a counter-clockwise direction.
	bool isInside(const SpatialVector& v, const SpatialVector& v0, const SpatialVector& v1, const SpatialVector& v2) const;

	// return the actual vertex vectors
	void nodeVertex(const signed __int64 id, int BaseIndex, int level, SpatialVector& v0, SpatialVector& v1, SpatialVector& v2) const;

	friend class SpatialEdge;
};