//#     Filename:       SpatialEdge.cpp
//#
//#     The SpatialEdge class is defined here.
//#
//#     Author:         Peter Z. Kunszt based on A. Szalay's code
//#     
//#     Date:           October 15, 1998
//#
//#		Copyright (C) 2000  Peter Z. Kunszt, Alex S. Szalay, Aniruddha R. Thakar
//#                     The Johns Hopkins University
//#
//#     Modification History:
//#
//#     Oct 18, 2001 : Dennis C. Dinge -- Replaced ValVec with std::vector
//#

#include "stdafx.h"
#include "SpatialEdge.h"

#define V(x) tree.vertices[tree_.nodes_[index].v[(x)]]
#define IV(x) tree.nodes[index].v[(x)]
#define W(x) tree.vertices[tree_.nodes_[index].w[(x)]]
#define IW(x) tree.nodes[index].w[(x)]
#define LAYER tree.layers[layerindex_]

/* SpatialEdge:
	constructor of the edge object */
SpatialEdge::SpatialEdge(SpatialIndex & tree_, size_t layerindex) : tree(tree_), layerindex_(layerindex)
{
	edges_ = new Edge[LAYER.nEdge + 1];
	lTab_ = new Edge*[LAYER.nVert * 6];

	// initialize lookup table, we depend on that NULL
	for (size_t i = 0; i < LAYER.nVert * 6; i++)
		lTab_[i] = NULL;

	// first vertex index for the vertices to be generated
	index_ = LAYER.nVert;
}

/* ~SpatialEdge:
	destructor of the edge object */
SpatialEdge::~SpatialEdge()
{
	delete[] edges_;
	delete[] lTab_;
}

/* makeMidPoints: 
	interface to this class. Set midpoints of every node in this layer */
void SpatialEdge::makeMidPoints()
{
	size_t c = 0;
	size_t index;

	index = (size_t)LAYER.firstIndex;
	for (long long i = 0; i < LAYER.nNode; i++, index++)
	{
		c = newEdge(c, index, 0);
		c = newEdge(c, index, 1);
		c = newEdge(c, index, 2);
	}
}

/* newEdge:
	Makes a new edge and stores it in the edges_ arrays element point to by the given emindex variable and in 
	the nodes array pointed to by the given index 
		> emindex - index of the edges_ array, the new edge will be stored in the array element pointed by this index
		> index - index of the node which has the new edge
		> k - the new edge will be one of the sides of the triangle, this int points to the side */
size_t SpatialEdge::newEdge(size_t emindex, size_t index, int k)
{
	Edge *en, *em;
	size_t swap;

	em = &edges_[emindex];

	switch (k)
	{
		case 0:
			em->start_ = IV(1);
			em->end_ = IV(2);
			break;
		case 1:
			em->start_ = IV(0);
			em->end_ = IV(2);
			break;
		case 2:
			em->start_ = IV(0);
			em->end_ = IV(1);
			break;
	}

	// sort the vertices by increasing index
	if (em->start_ > em->end_)
	{
		swap = em->start_;
		em->start_ = em->end_;
		em->end_ = swap;
	}

	// check all previous edges for a match, return pointer if 
	// already present, log the midpoint with the new face as well
	if ((en = edgeMatch(em)) != NULL)
	{
		IW(k) = en->mid_;
		return emindex;
	}

	// this is a new edge, immediately process the midpoint, 
	// and save it with the nodes and the edge as well
	insertLookup(em);
	IW(k) = getMidPoint(em);
	em->mid_ = IW(k);

	return ++emindex;
}

/* insertLookup:
	Insert the edge em into the lookup table. indexed by em->start_.
	Every vertex has at most 6 edges, so only that much lookup needs to be done 
		> em - edge to be inserted in to the lookup table */
void SpatialEdge::insertLookup(Edge * em)
{
	size_t j = 6 * em -> start_;
	int i;

	// do not loop beyond 6
	for (i = 0; i < 6; i++, j++)
	{
		if (lTab_[j] == NULL)
		{
			lTab_[j] = em;
			return;
		}
	}
}

#if defined(__sun) && !defined(__gnu)
Edge *
#else
SpatialEdge::Edge *
#endif

/* edgeMatch:
	Fast lookup using the first index em->start_. 
	Return pointer to edge if matches, null if not. 
		> em - edge we are looking up */
SpatialEdge::edgeMatch(Edge *em)
{
	size_t i = 6 * em -> start_;

	while (lTab_[i] != NULL)
	{
		if (em->end_ == lTab_[i]->end_)
			return lTab_[i];
		i++;
	}

	return NULL;
}

/* getMidPoint:
	compute the midpoint of the edge using vector algebra and return its index in the vertex list 
		> em - we are getting the mid point of this edge*/
size_t SpatialEdge::getMidPoint(Edge * em)
{
	tree.vertices[index_] = tree.vertices[em->start_] + tree.vertices[em->end_];
	tree.vertices[index_].normalize();

	return index_++;
}