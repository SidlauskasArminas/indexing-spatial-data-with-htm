//#     Filename:       SpatialVector.h
//#
//#     Standard 3-d vector class
//#
//#     Author:         Peter Z. Kunszt, based on A. Szalay's code
//#     
//#     Date:           October 15, 1998
//#
//#		Copyright (C) 2000  Peter Z. Kunszt, Alex S. Szalay, Aniruddha R. Thakar
//#                     The Johns Hopkins University
//#
//#     Modification History:
//#
//#     Oct 18, 2001 : Dennis C. Dinge -- Replaced ValVec with std::vector
//#

#pragma once

#include <math.h>
#include <stdio.h>
#include <iostream>

//Earth globe constants
const double equator_radius = 6378.1370;
const double poles_radius = 6356.752314245;
const double prec = 0.0000001;

/*
   The SpatialVector is a 3D vector usually living on the surface of
   the sphere. The corresponding ra, dec can be obtained if the vector
   has unit length. That can be ensured with the normalize() function.
*/
class SpatialVector
{
public:
	//CONSTRUCTORS

	// Constructs (1, 0, 0)
	SpatialVector();

	// Constructor from xyz coordinates, not necessarily normed to 1
	SpatialVector(double x, double y, double z);

	// Constructor from latitude and longtitude. Calls LatLonToXyz function 
	SpatialVector(double latitude, double longtitude);
	
	// Constructor from anotehr spatial vector, copies it
	SpatialVector(const SpatialVector & vv);

	//FUNCTIONS

	// Set member function: set values - always normed to 1
	void set(const double &x, const double &y, const double &z);

	// Gets x,y,z
	void get(double & x, double & y, double & z) const;

	// Return length of vector
	double length() const;

	// Normalize vector length to 1
	void normalize();

	// Prints out the vectors coordinates
	void show() const;

	// Converts lattitude and longtitude to xyz
	void LatLonToXyz(double latitude, double longtitude);

	//OPERATOR OVERRIDE

	// Assignment
	SpatialVector& operator =(const SpatialVector &);

	// Comparison
	int operator ==(const SpatialVector &) const;

	// Operator *= with double
	SpatialVector & operator *=(double);

	// Operator *= with int
	SpatialVector & operator *=(int);

	// Dot product
	double operator *(const SpatialVector &) const;

	// Cross product
	SpatialVector operator ^(const SpatialVector &) const;

	// Addition
	SpatialVector operator +(const SpatialVector &) const;

	// Subtraction
	SpatialVector operator -(const SpatialVector &) const;

	friend SpatialVector operator *(double, const SpatialVector &);
	friend SpatialVector operator *(int, const SpatialVector &);

private:
	double x;
	double y;
	double z;

	double lat;
	double lon;

	friend class SpatialIndex;
};