//#     Filename:       SpatialVector.cpp
//#
//#     The SpatialVector class is defined here.
//#
//#     Author:         Peter Z. Kunszt based on A. Szalay's code
//#
//#     Date:           October 15, 1998
//#
//#		Copyright (C) 2000  Peter Z. Kunszt, Alex S. Szalay, Aniruddha R. Thakar
//#                     The Johns Hopkins University
//#     Modification History:
//#
//#     Oct 18, 2001 : Dennis C. Dinge -- Replaced ValVec with std::vector
//#

#include "stdafx.h"
#include "SpatialVector.h"
#include "SpatialIndex.h"

// This 3D vector lives on the surface of the sphere.
// Its length is always 1.

//CONSTRUCTORS

/* 1) SpatialVector:
	Constructs (1, 0, 0) vector */
SpatialVector::SpatialVector() : x(1), y(0), z(0)
{
}

/* 2) SpatialVector:
	Constructor from xyz coordinates 
		> x, y, z - coordinates of a point */
SpatialVector::SpatialVector(double x_, double y_, double z_) : x(x_), y(y_), z(z_)
{
}

/* 3) SpatialVector:
	Constructor from latitude and longtitude 
		> latitude, longtitude - coordinates of a point */
SpatialVector::SpatialVector(double latitude, double longtitude)
{
	lat = latitude;
	lon = longtitude;
	LatLonToXyz(latitude, longtitude);
}

/* LatLonToXyz:
	Converts lattitude and longtitude to xyz
		> latitude, longtitude - coordinates of a point */
void SpatialVector::LatLonToXyz(double latitude, double longtitude)
{
	if (latitude > 90 || latitude < -90) {
		throw std::out_of_range("LatLonToXyz: Invalid latitude. It is outside of its range, which is from -90.0 (inclusive)to 90.0 (inclusive)");
	}
	
	if (longtitude > 180 || longtitude < -180) {
		throw std::out_of_range("LatLonToXyz: Invalid longitude. It is outside of its range, which is from -180.0 (inclusive) to 180.0 (inclusive)");
	}

	double latRadian = gPi * latitude / 180.0;
	double lonRadian = gPi * longtitude / 180.0;
	double N = pow(equator_radius, 2) / sqrt(pow(equator_radius*cos(latRadian), 2) + pow(poles_radius*sin(latRadian), 2));
	x = N * cos(latRadian) * cos(lonRadian);
	y = N * cos(latRadian) * sin(lonRadian);
	z = pow(poles_radius / equator_radius, 2) * N * sin(latRadian);
}

/* 4) SpatialVector:
	Constructor from another vector, copies it
		> vv - another vector, its coordinates will be set to the new vector */
SpatialVector::SpatialVector(const SpatialVector & vv) : x(vv.x), y(vv.y), z(vv.z)
{
}

//FUNCTIONS

/* set:
	Set member function: set values - always normed to 1 
		> x_, y_, z_ - coordinates that are set to the vector */
void SpatialVector::set(const double & x_, const double & y_, const double & z_)
{
	x = x_;
	y = y_;
	z = z_;
	normalize();
}

/* get:
	Gets x,y,z coordinates of a vector
		> x_, y_, z_ - coordinates of the vector that are returned */
void SpatialVector::get(double & x_, double & y_, double & z_) const
{
	x_ = x;
	y_ = y;
	z_ = z;
}

/* lenght:
	Return length of vector */
double SpatialVector::length() const
{
	double sum;
	sum = x * x + y * y + z * z;

	return sum > gEpsilon ? sqrt(sum) : 0.0;
}

/* normalize:
	Normalize vector length to 1 */
void SpatialVector::normalize()
{
	double sum = x * x + y * y + z * z;
	double num = sum - 1.0;
	if (num > 4.0 * pow(2.0, -53.0) || num < 0.0 - 4.0 * pow(2.0, -53.0))
	{
		double num2 = sqrt(sum);
		x /= num2;
		y /= num2;
		z /= num2;
	}
}

/* show:
	Prints out the vectors coordinates */
void SpatialVector::show() const
{
	printf(" %11.8f %11.8f %11.8f \n", x, y, z);
}

//OPERATOR OVERRIDE FOR ACTIONS WITH SPATIAL VECTORS

SpatialVector& SpatialVector::operator =(const SpatialVector& vv)
{
	x = vv.x;
	y = vv.y;
	z = vv.z;

	return *this;
}

SpatialVector& SpatialVector::operator *=(double a)
{
	x = a * x;
	y = a * y;
	z = a * z;

	return *this;
}

SpatialVector& SpatialVector::operator *=(int a)
{
	x = a * x;
	y = a * y;
	z = a * z;

	return *this;
}

int SpatialVector::operator ==(const SpatialVector & v) const
{

	if (fabs(x - v.x) < prec && fabs(y - v.y) < prec && fabs(z - v.z) < prec)
		return 1;
	return 0;
}

SpatialVector operator *(double a, const SpatialVector& v)
{
	return SpatialVector(a*v.x, a*v.y, a*v.z);
}

SpatialVector operator *(int a, const SpatialVector& v)
{
	return SpatialVector(a*v.x, a*v.y, a*v.z);
}

double SpatialVector::operator *(const SpatialVector & v) const
{
	return (x*v.x) + (y*v.y) + (z*v.z);
}

SpatialVector SpatialVector::operator ^(const SpatialVector & v) const
{
	return SpatialVector(y * v.z - v.y * z, z * v.x - v.z * x, x * v.y - v.x * y);
}

SpatialVector SpatialVector::operator +(const SpatialVector & v) const
{
	return SpatialVector(x + v.x, y + v.y, z + v.z);
}

SpatialVector SpatialVector::operator -(const SpatialVector & v) const
{
	return SpatialVector(x - v.x, y - v.y, z - v.z);
}