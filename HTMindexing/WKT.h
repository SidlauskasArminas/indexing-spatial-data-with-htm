#pragma once

#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

class WKT
{
public:
	// Constructor for the WKT class
	WKT();

	// Returns a POINT string with the given level node index in which the given coordinates are
	static string getPoint(double x, double y, double z, int level);

	// Returns a MULTIPOINT string with the node indexes for every given coordinate
	static string getMultiPoint(const int NumberOfCoordinates, double coordinates[][3], int level);

	// Returns a LINE string with the given level node indexes, these nodes create the line.
	// NumberOfCoordiantes - gives the number of xyz coordintes in the array that create the line
	static string getLineString(const int NumberOfCoordinates, double coordinates[][3], int level);

	// Returns a POLYGON string with the indexes of node created with the given coordinates and level
	static string getPolygon(const int NumberOfCoordinates, double coordinates[][3], int level);
};

